import { Component, OnInit } from '@angular/core';
import { EndUser } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-smart-meter-user',
  templateUrl: './smart-meter-user.component.html',
  styleUrls: ['./smart-meter-user.component.css']
})
export class SmartMeterUserComponent implements OnInit {
  users: EndUser[] = [];

  constructor(private energyService: EnergyService) { }

  ngOnInit(): void {
    this.energyService.getAllUsers().subscribe((res: any) => {
      (res.data).forEach((element: any) => {
        this.users.push(new EndUser(element.id, element.name, element.mailId, element.password, element.smartMeters));
      })
    })
  }

}
