import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartMeterUserComponent } from './smart-meter-user.component';

describe('SmartMeterUserComponent', () => {
  let component: SmartMeterUserComponent;
  let fixture: ComponentFixture<SmartMeterUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmartMeterUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartMeterUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
