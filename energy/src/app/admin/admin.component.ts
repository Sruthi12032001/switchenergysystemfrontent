import { Component, OnInit } from '@angular/core';
import { Admin } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  admins: Admin[] = [];

  constructor(private energyService: EnergyService) { }

  ngOnInit(): void {
    this.energyService.getAllAdmins().subscribe((res: any) => {
       (res.data).forEach((element: Admin) => {
        this.admins.push(new Admin(element.mail, element.password));
      });
    })
  }

}
