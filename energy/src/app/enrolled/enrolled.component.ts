import { Component, OnInit } from '@angular/core';
import { SmartMeters } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-enrolled',
  templateUrl: './enrolled.component.html',
  styleUrls: ['./enrolled.component.css']
})
export class EnrolledComponent implements OnInit {

  smartMeters : SmartMeters[] = []; 
  constructor(private energyService: EnergyService) { }

  ngOnInit(): void {
    this.energyService.getSmartMetersEnrolled().subscribe((res: any) => {
      (res.data).forEach((element: any) => {
        this.smartMeters.push(new SmartMeters(element.id, element.smartMeterId, element.status, element.holder, element.datas, element.provider, element.endUserId));
      });
    })
  }

  action(i: number, status : string) {
    const smartMeter = this.smartMeters[i];
    this.energyService.changeSmartMeterStatus({"id" : smartMeter.id, "smartMeterId": smartMeter.smartMeterId, "holder" : smartMeter.holder, "status" : !smartMeter.status, "provider" : smartMeter.provider, "endUserId" :  smartMeter.endUserId, "data" : smartMeter.data}).subscribe((res: any) => {
      const element = res.data;
      this.smartMeters[i] = new SmartMeters(element.id, element.smartMeterId, element.status, element.holder, element.datas, element.provider, element.endUserId);
    }) 
  }

}
