import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEndUserComponent } from './add-end-user.component';

describe('AddEndUserComponent', () => {
  let component: AddEndUserComponent;
  let fixture: ComponentFixture<AddEndUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEndUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEndUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
