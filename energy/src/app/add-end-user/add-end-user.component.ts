import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-add-end-user',
  templateUrl: './add-end-user.component.html',
  styleUrls: ['./add-end-user.component.css']
})
export class AddEndUserComponent implements OnInit {
  mail !: string;
  password !: string;
  name !: string;
  type !: string;
  mailRegex = new RegExp('[a-zA-Z0-9][a-zA-Z0-9.]+@[a-zA-Z0-9]+([.][a-zA-Z]+)+');
  passwordRegex = new RegExp('(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[%$@!]).{8,20}');
  nameRegex =  new RegExp('[A-Za-z\\s]{3,16}');

  constructor(private energyService: EnergyService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.type = this.route.snapshot.params['type'];
  }

  create() {
      if(this.type === 'User') {
        if(this.name != undefined && this.name.match(this.nameRegex) && this.password != undefined && this.password.match(this.passwordRegex) && this.mail != undefined && this.mail.match(this.mailRegex)) {
          this.energyService.addUser({"name" : this.name, "mailId" : this.mail, "password" : this.password}).subscribe((res: any) => {
            this.router.navigate(['displayadmin/viewProviders']);
          });
        } 
      } else if(this.type === 'Admin') {
        if(this.password != undefined && this.password.match(this.passwordRegex) && this.mail != undefined && this.mail.match(this.mailRegex)) {
          this.energyService.addAdmin({"mail" : this.mail, "password" : this.password}).subscribe((res: any) => {
            this.router.navigate(['displayadmin/viewProviders']);
          })
        }
      }
    }

}
