import { EnergyService } from "../service/energy.service";

export class Admin {
    mail: string;
    password: string;
    constructor(mail: string, password: string) {
        this.mail = mail;
        this.password = password;
    }
}
export class Providers {
    id: string;
    name: string;
    amountPerKwh: number;
    enabled: boolean;
    constructor(id: string, name: string, amountPerKwh: number, enabled: boolean) {
        this.id = id;
        this.name = name;
        this.amountPerKwh = amountPerKwh;
        this.enabled = enabled;
    } 
}



export class SmartMeters {
    id: string;
    smartMeterId: string;
    data: number[];
    holder: string;
    status: string;
    provider: string;
    endUserId: string;
    totalKWH: number;
    calculate = false;
    value = "0";

    constructor(id : string, smartMeterId: string, status: string, holder: string, data: number[] = [], provider: string, endUserId : string, totalKWH: number = 0) {
        this.id = id;
        this.smartMeterId = smartMeterId;
        this.holder = holder;
        this.data = data;
        this.status = status;
        this.provider = provider;
        this.endUserId = endUserId;
        this.totalKWH = totalKWH;
    } 
}

export class EndUser {
    id: string;
    name : string;
    mail : string;
    password : string;
    smartMeters : number[];
    constructor(id: string, name: string, mail: string, password: string, smartMeters : number[]) {
        this.id = id;
        this.name = name;
        this.mail = mail;
        this.password = password;
        this.smartMeters = smartMeters;
    }
}
