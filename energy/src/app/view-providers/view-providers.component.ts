import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Providers } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-view-providers',
  templateUrl: './view-providers.component.html',
  styleUrls: ['./view-providers.component.css']
})
export class ViewProvidersComponent implements OnInit {
  providers: Providers[] = [];

  constructor(private energyService: EnergyService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.energyService.getProviders().subscribe((res: any) => {
        (res.data).forEach((element: any) => {
          this.providers.push(new Providers(element.id, element.name, element.amountPerKwh, element.enabled));
        });
      })
  }

   action(i: number) {
      const provider = this.providers[i];
      provider.enabled = !provider.enabled;
      this.energyService.changeProviderStatus(provider).subscribe((res: any) => {
        const element = res.data;
        this.providers[i] = new Providers(element.id, element.name, element.amountPerKwh, element.enabled);
      }) 
    }

}
