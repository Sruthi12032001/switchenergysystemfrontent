import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-add-reading',
  templateUrl: './add-reading.component.html',
  styleUrls: ['./add-reading.component.css']
})
export class AddReadingComponent implements OnInit {
  reading: number = 0;
  smartMeterId : string = '';
  userId: string = '';

  constructor(private route: ActivatedRoute, private router: Router, private energyService: EnergyService) { 
    this.smartMeterId = this.route.snapshot.params['smartMeterId'];
    this.userId = this.route.snapshot.params['userId'];
    console.log(this.smartMeterId);
  }

  ngOnInit(): void {
  }

  back() {
    this.router.navigate(['endUserDisplay', this.userId]);
  }

  addReading() {
    this.energyService.addReading(this.reading, this.smartMeterId).subscribe((res: any) => {
      this.router.navigate(['endUserDisplay', this.userId]);
    })
  }


}
