import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Admin, EndUser, Providers, SmartMeters } from '../constant/admin';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EnergyService {

  constructor(private http: HttpClient) { }

  private getToken(): HttpHeaders {
    const token = new HttpHeaders({
      Authorization: localStorage.getItem('jwttoken') + '',
    });
    return token;
  }
  
  adminLogin(object: Admin): Observable<any> {
    return this.http.post(`${environment.baseUrl}admin/login`, object, {
      observe: 'response',
    });
  }


  //view providers
  getProviders(): Observable<any> {
    return this.http.get(`${environment.baseUrl}provider`, { headers: this.getToken() });
  }

  changeProviderStatus(object: Providers): Observable<any> {
    return this.http.put(`${environment.baseUrl}provider/changeProviderStatus/${object.id}`, object, { headers: this.getToken() });
  }


  //enrolled providers
  getSmartMetersEnrolled() : Observable<any> {
    return this.http.get(`${environment.baseUrl}smartMeter/smartMetersEnrolled`, { headers: this.getToken() });
  }

  changeSmartMeterStatus(object: any) : Observable<any> {
    return this.http.put(`${environment.baseUrl}smartMeter/changeSmartMeterStatus/${object.id}`, object, { headers: this.getToken() });
  }



  //new smart meter
  getNewSmartMeters() : Observable<any> {
    return this.http.get(`${environment.baseUrl}smartMeter/newSmartMeters`, { headers: this.getToken() });
  }

  addSmartMeter(object: any) : Observable<any> {
    return this.http.post(`${environment.baseUrl}smartMeter/addSmartMeter`, object, { headers: this.getToken() });
  }

  rejectSmartMeter(object: any) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}deleteSmartUser/${object.id}`);
  }


  //addProvider
  addProvider(object: any) : Observable<any> {
    return this.http.post(`${environment.baseUrl}provider/addProvider`, object, { headers: this.getToken() });
  }

  addUser(object: any) : Observable<any> {
    return this.http.post(`${environment.baseUrl}user/addUser`, object, { headers: this.getToken() });
  }

  //addAdmin
  addAdmin(object: Admin) : Observable<any> {
    return this.http.post(`${environment.baseUrl}admin/addAdmin`, object, { headers: this.getToken() })
  }

  //userlogin
  endUserLogin(object: any) : Observable<any> {
    return this.http.post(`${environment.baseUrl}user/login`, object, {
      observe: 'response',
    });
  }

  //get Admin
  getAllAdmins() : Observable<any>{
    return this.http.get(`${environment.baseUrl}admin/getAll`, { headers: this.getToken() });
  }

  //get all users
  getAllUsers() : Observable<any> {
    return this.http.get(`${environment.baseUrl}user/getAll`, { headers: this.getToken() });
  }

  getUserById(id : string) : Observable<any> {
    return this.http.get(`${environment.baseUrl}user/getById/${id}`, { headers: this.getToken() });
  }

  getAllSmartMeters(id: string) : Observable<any> {
    return this.http.get(`${environment.baseUrl}user/getAllSmartMeters/${id}`, { headers: this.getToken() });
  }

  calculate(id : string) : Observable<any> {
    return this.http.get(`${environment.baseUrl}calculate/${id}`, { headers: this.getToken() });
  }

  getProviderByName(name : string) : Observable<any> {
    return this.http.get(`${environment.baseUrl}provider/getProviderByName/${name}`, { headers: this.getToken() });
  }

  getReadings(id : string) : Observable<any> {
    return this.http.get(`${environment.baseUrl}smartMeter/getReadings/${id}`, { headers: this.getToken() });
  }

  changeProvider(object:  Providers, id: string) : Observable<any> {
    return this.http.put(`${environment.baseUrl}smartMeter/changeProvider/${id}`, object, { headers: this.getToken() });
  }

  addReading(reading: number, id: string) : Observable<any> {
    return this.http.put(`${environment.baseUrl}smartMeter/readings/${id}/${reading.toString()}`, {}, { headers: this.getToken() });
  }

  addNewSmartMeter(object: any) : Observable<any> {
    return this.http.post(`${environment.baseUrl}smartMeter/addNewSmartMeter`, object, { headers: this.getToken() });
  }

}
