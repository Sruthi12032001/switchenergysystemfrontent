import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSmartMeterUsersComponent } from './new-smart-meter-users.component';

describe('NewSmartMeterUsersComponent', () => {
  let component: NewSmartMeterUsersComponent;
  let fixture: ComponentFixture<NewSmartMeterUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewSmartMeterUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSmartMeterUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
