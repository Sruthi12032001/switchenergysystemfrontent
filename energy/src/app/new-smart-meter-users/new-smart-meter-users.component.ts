import { Component, OnInit } from '@angular/core';
import { SmartMeters } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-new-smart-meter-users',
  templateUrl: './new-smart-meter-users.component.html',
  styleUrls: ['./new-smart-meter-users.component.css']
})
export class NewSmartMeterUsersComponent implements OnInit {

  smartMeters: SmartMeters[] = [];
  constructor(private energyService: EnergyService) { }

  ngOnInit(): void {
    this.energyService.getNewSmartMeters().subscribe((res: any) => {
      
      (res.data).forEach((element: any) => {
        this.smartMeters.push(new SmartMeters(element.id, element.smartMeterId, element.status, element.holder, element.data, element.provider, element.endUserId));
      });
    })
  }

  actionEnroll(i: number) {
    const smartMeter = this.smartMeters[i];
    this.energyService.addSmartMeter({"id" : smartMeter.id, "smartMeterId": smartMeter.smartMeterId, "holder" : smartMeter.holder, "status" : smartMeter.status, "provider" : smartMeter.provider, "endUserId" :  smartMeter.endUserId, "data" : smartMeter.data}).subscribe((res: any) => {
      this.smartMeters = [];
      (res.data).forEach((element: any) => {
        this.smartMeters.push(new SmartMeters(element.id, element.smartMeterId, element.status, element.holder, element.data, element.provider, element.endUserId));
      });
    });
  }

  actionReject( i: number) {
    const smartMeter = this.smartMeters[i];
    this.energyService.rejectSmartMeter({"id" : smartMeter.id, "smartMeterId": smartMeter.smartMeterId, "holder" : smartMeter.holder, "status" : smartMeter.status, "provider" : smartMeter.provider, "endUserId" :  smartMeter.endUserId, "data" : smartMeter.data}).subscribe((res: any) => {
      this.smartMeters = [];
      (res.data).forEach((element: any) => {
        this.smartMeters.push(new SmartMeters(element.id, element.smartMeterId, element.status, element.holder, element.data, element.provider, element.endUserId));
      });
    });
  }
}
