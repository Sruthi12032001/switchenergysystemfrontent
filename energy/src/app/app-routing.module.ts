import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEndUserComponent } from './add-end-user/add-end-user.component';
import { AddReadingComponent } from './add-reading/add-reading.component';
import { AddSmartmeterComponent } from './add-smartmeter/add-smartmeter.component';
import { AdminComponent } from './admin/admin.component';
import { DislayforadminComponent } from './dislayforadmin/dislayforadmin.component';
import { EndUserComponent } from './end-user/end-user.component';
import { EnrolledComponent } from './enrolled/enrolled.component';
import { HomeComponent } from './home/home.component';
import { NewProviderComponent } from './new-provider/new-provider.component';
import { NewSmartMeterUsersComponent } from './new-smart-meter-users/new-smart-meter-users.component';
import { SmartMeterUserComponent } from './smart-meter-user/smart-meter-user.component';
import { ViewProvidersEnduserComponent } from './view-providers-enduser/view-providers-enduser.component';
import { ViewProvidersComponent } from './view-providers/view-providers.component';
import { ViewReadingComponent } from './view-reading/view-reading.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'displayadmin', component: DislayforadminComponent, children: [
    {path: 'addProvider', component: NewProviderComponent},
    {path: 'enrolled', component: EnrolledComponent},
    {path: 'newSmartMeters', component: NewSmartMeterUsersComponent},
    {path: 'viewProviders', component: ViewProvidersComponent},
    {path: 'addUser/:type', component: AddEndUserComponent},
    {path: 'addAdmin/:type', component: AddEndUserComponent},
    {path: 'smartMeterUsers', component: SmartMeterUserComponent},
    {path: 'adminsDisplay', component: AdminComponent}
  ]},
  {path: 'displayadmin/viewProviders', component: ViewProvidersComponent},
  {path: 'displayadmin/addUser', component: AddEndUserComponent},
  {path: 'viewReadings/:smartMeterId/:userId', component: ViewReadingComponent},
  {path: 'switchProviders/:smartMeterId/:name/:userId', component: ViewProvidersEnduserComponent},
  {path: 'endUserDisplay/:id', component: EndUserComponent},
  {path: 'addReading/:smartMeterId/:userId', component: AddReadingComponent},
  {path: 'addSmartMeter/:id', component: AddSmartmeterComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
