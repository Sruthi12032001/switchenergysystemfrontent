import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  mail = "";
  password = "";
  mailRegex = new RegExp('[a-zA-Z0-9][a-zA-Z0-9.]+@[a-zA-Z0-9]+([.][a-zA-Z]+)+');

  constructor(private energyService: EnergyService, private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    this.energyService.adminLogin({"mail" : this.mail, "password" : this.password}).subscribe((res: any) => {
      localStorage.setItem("jwttoken", res.headers.get('jwttoken'));
      this.router.navigate(['/displayadmin']);
    }, (err: any) => {
      console.log(err.error.message);
      if(err.error.message === "Enter valid mail id") {
        this.energyService.endUserLogin({"mailId" : this.mail, "password" : this.password}).subscribe((res: any) => {
          localStorage.setItem("jwttoken", res.headers.get('jwttoken'));
            this.router.navigate(['/endUserDisplay', res.data[0].id]);
          })
      }
    })
  }

}
