import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { DislayforadminComponent } from './dislayforadmin/dislayforadmin.component';
import { EnrolledComponent } from './enrolled/enrolled.component';
import { NewProviderComponent } from './new-provider/new-provider.component';
import { ViewProvidersComponent } from './view-providers/view-providers.component';
import { NewSmartMeterUsersComponent } from './new-smart-meter-users/new-smart-meter-users.component';
import { EndUserComponent } from './end-user/end-user.component';
import { AddEndUserComponent } from './add-end-user/add-end-user.component';
import { SmartMeterUserComponent } from './smart-meter-user/smart-meter-user.component';
import { AdminComponent } from './admin/admin.component';
import { ViewReadingComponent } from './view-reading/view-reading.component';
import { ViewProvidersEnduserComponent } from './view-providers-enduser/view-providers-enduser.component';
import { AddReadingComponent } from './add-reading/add-reading.component';
import { AddSmartmeterComponent } from './add-smartmeter/add-smartmeter.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DislayforadminComponent,
    EnrolledComponent,
    NewProviderComponent,
    ViewProvidersComponent,
    NewSmartMeterUsersComponent,
    EndUserComponent,
    AddEndUserComponent,
    SmartMeterUserComponent,
    AdminComponent,
    ViewReadingComponent,
    ViewProvidersEnduserComponent,
    AddReadingComponent,
    AddSmartmeterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
