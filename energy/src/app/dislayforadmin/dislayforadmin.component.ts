import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Providers } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-dislayforadmin',
  templateUrl: './dislayforadmin.component.html',
  styleUrls: ['./dislayforadmin.component.css']
})
export class DislayforadminComponent implements OnInit {
  providers : Providers[] = []; 
  type !: string;
  constructor(private energyService: EnergyService, private router: Router, private route: ActivatedRoute) { }

  

  selectType(type: string) {
    switch(type) {
      case "addProvider":
        this.router.navigate(['addProvider'], {relativeTo: this.route});
        break;
      case "viewProviders":
        this.router.navigate(['viewProviders'], {relativeTo: this.route});
        break;
      case "enrolledSmartMeters":
        this.router.navigate(['enrolled'], {relativeTo: this.route});
        break;
      case "newSmartMeters":
        this.router.navigate(['newSmartMeters'], {relativeTo: this.route});
        break;
      case "addEndUser":
        this.router.navigate(['addUser', 'User'], {relativeTo: this.route});
        break;
      case "addAdmin":
        this.router.navigate(['addAdmin', 'Admin'], {relativeTo: this.route});
        break;
      case "smartMeterUsers":
        this.router.navigate(['smartMeterUsers'], {relativeTo: this.route});
        break;
      case "admins":
        this.router.navigate(['adminsDisplay'], {relativeTo: this.route});
        break;
    }
  }

  ngOnInit(): void {
    this.router.navigate(['viewProviders'], {relativeTo: this.route});
  }

}
