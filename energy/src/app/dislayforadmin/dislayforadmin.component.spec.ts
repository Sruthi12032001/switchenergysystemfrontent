import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DislayforadminComponent } from './dislayforadmin.component';

describe('DislayforadminComponent', () => {
  let component: DislayforadminComponent;
  let fixture: ComponentFixture<DislayforadminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DislayforadminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DislayforadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
