import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { elementAt } from 'rxjs';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-view-reading',
  templateUrl: './view-reading.component.html',
  styleUrls: ['./view-reading.component.css']
})
export class ViewReadingComponent implements OnInit {
  readings: number[] = [];
  smartMeterId: string = '';
  userId: string = '';
 
  constructor(private energyService: EnergyService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.smartMeterId = this.route.snapshot.params['smartMeterId'];
    console.log(this.smartMeterId);
    
    this.userId = this.route.snapshot.params['userId'];
    this.energyService.getReadings(this.smartMeterId).subscribe((res) => {
      (res.data).forEach((element : number) => {
        this.readings.push(element);
      })
    })
  }

  navigateBack() {
    this.router.navigate(['endUserDisplay', this.userId]);
  }

}
