import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EndUser, SmartMeters } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-end-user',
  templateUrl: './end-user.component.html',
  styleUrls: ['./end-user.component.css']
})
export class EndUserComponent implements OnInit {
  smartMeters: SmartMeters[] = [];
  id: string = '';

  constructor(private energyService: EnergyService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.energyService.getAllSmartMeters(this.id).subscribe((res) => {
      (res.data).forEach((element: any) => {
        this.energyService.calculate(element.smartMeterId).subscribe((res: any) => {
          this.smartMeters.push(new SmartMeters(element.id, element.smartMeterId, element.status, element.holder, element.data, element.provider, element.endUserId, res.data === null ? 0 : res.data.toFixed(4)));
        })
      });
    })
  }

  calculate(i: number) {
    if(this.smartMeters[i].calculate) {
      this.smartMeters[i].calculate = !this.smartMeters[i].calculate;
    } else{
      this.energyService.getProviderByName(this.smartMeters[i].provider).subscribe((res: any) => {
        this.smartMeters[i].value = (res.data[0].amountPerKwh * this.smartMeters[i].totalKWH).toFixed(4);
        this.smartMeters[i].calculate = true;
      })
    }
    
  }

  addReading(i : number) {
    this.router.navigate(['addReading', this.smartMeters[i].smartMeterId, this.id]);
  }

  switchProvider(i : number) {
    this.router.navigate(['switchProviders', this.smartMeters[i].smartMeterId, this.smartMeters[i].provider, this.id]);
  }

  viewReadings(i : number) {
    this.router.navigate(['viewReadings', this.smartMeters[i].smartMeterId, this.id])
  }

  addSmartMeter() {
    this.router.navigate(['addSmartMeter', this.id]);
  }
 


}
