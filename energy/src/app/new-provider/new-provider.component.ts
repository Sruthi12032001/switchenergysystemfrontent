import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-new-provider',
  templateUrl: './new-provider.component.html',
  styleUrls: ['./new-provider.component.css']
})
export class NewProviderComponent implements OnInit {
  name !: string;
  amount !: number;
  nameRegex =  new RegExp('[A-Za-z\\s]{3,16}');
  constructor(private energyService: EnergyService, private router: Router) { }

  ngOnInit(): void {
  }

  create() {
    if(this.name != undefined && this.name.match(this.nameRegex)) {
      this.energyService.addProvider({"name" : this.name,"amountPerKwh" : this.amount}).subscribe((res: any) => {
        this.router.navigate(['displayadmin/viewProviders']);
      });
    }
  }

}
