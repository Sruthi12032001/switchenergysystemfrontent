import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProvidersEnduserComponent } from './view-providers-enduser.component';

describe('ViewProvidersEnduserComponent', () => {
  let component: ViewProvidersEnduserComponent;
  let fixture: ComponentFixture<ViewProvidersEnduserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewProvidersEnduserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProvidersEnduserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
