import { Component, OnInit, Provider } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Providers } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-view-providers-enduser',
  templateUrl: './view-providers-enduser.component.html',
  styleUrls: ['./view-providers-enduser.component.css']
})
export class ViewProvidersEnduserComponent implements OnInit {
  smartMeterId: string = '';
  userId: string = '';
  name : string = '';
  alreadyUsed !: Providers;
  providers: Providers[] = [];
  constructor(private route: ActivatedRoute, private energyService: EnergyService, private router: Router) { 
    this.smartMeterId = this.route.snapshot.params['smartMeterId'];
    this.userId = this.route.snapshot.params['userId'];
    this.name = this.route.snapshot.params['name'];

    this.energyService.getProviders().subscribe((res: any) => {
      (res.data).forEach((element: any) => {
        if(element.name === this.name && element.enabled) {
          this.alreadyUsed = new Providers(element.id, element.name, element.amountPerKwh, element.enabled);
        } else if(element.enabled) {
          this.providers.push(new Providers(element.id, element.name, element.amountPerKwh, element.enabled));
        }
      })
    })
  }

  ngOnInit(): void {
  }

  switchProvider(i : number) {
    this.energyService.changeProvider(this.providers[i], this.smartMeterId).subscribe((res: any) => {
      this.router.navigate(['endUserDisplay', this.userId]);
    })
  }

}
