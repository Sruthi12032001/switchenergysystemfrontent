import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSmartmeterComponent } from './add-smartmeter.component';

describe('AddSmartmeterComponent', () => {
  let component: AddSmartmeterComponent;
  let fixture: ComponentFixture<AddSmartmeterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSmartmeterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSmartmeterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
