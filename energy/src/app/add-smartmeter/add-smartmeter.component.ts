import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-add-smartmeter',
  templateUrl: './add-smartmeter.component.html',
  styleUrls: ['./add-smartmeter.component.css']
})
export class AddSmartmeterComponent implements OnInit {
  smartMeterId = "";
  userId = "";
  providers : string[] = [];

  constructor(private energyService: EnergyService, private route: ActivatedRoute, private router: Router) { 
    this.userId = this.route.snapshot.params['id'];
    this.energyService.getProviders().subscribe((res) => {
      (res.data).forEach((ele: any) => {
        this.providers.push(ele.name);
      })
      
    })

  }

  ngOnInit(): void {
  }

  back() {
    this.router.navigate(['endUserDisplay', this.userId]);
  }

  create(value: string) {
    if(this.smartMeterId != "") {
      console.log(value);
      
      this.energyService.getUserById(this.userId).subscribe((res: any) => {
        this.energyService.addNewSmartMeter({"smartMeterId" : this.smartMeterId, "endUserId" : this.userId, "holder" : res.data.name, "provider" : value.trim()}).subscribe((res: any) => {
          this.router.navigate(['endUserDisplay', this.userId]);
        })

      })
    }
  }

}
